# Skite-API
The API backing the Skite website.

## Dependencies
Data is stored using `MongoDB`

## Start the project
1. Start the required dependencies using `docker-compose`:
```shell
docker-compose -f docker-compose-dev.yml up
```

2. Run the Spring boot app using the `dev` profile:
```shell
SPRING_PROFILES_ACTIVE=dev mvn spring-boot:run
```

3. Access the local instance: e.g. `localhost:8080/api/user`
