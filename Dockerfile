FROM openjdk:18-jdk-alpine

RUN apk update && apk add maven
COPY . /project
RUN  cd /project && mvn package

ENTRYPOINT ["java", "-jar","/project/target/SkiteApi-0.0.1-SNAPSHOT.jar"]
