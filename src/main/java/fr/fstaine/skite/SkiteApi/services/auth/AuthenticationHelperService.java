package fr.fstaine.skite.SkiteApi.services.auth;

public interface AuthenticationHelperService {
    boolean isLoggedIn();
}
