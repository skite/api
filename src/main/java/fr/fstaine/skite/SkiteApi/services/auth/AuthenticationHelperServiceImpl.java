package fr.fstaine.skite.SkiteApi.services.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationHelperServiceImpl implements AuthenticationHelperService {

    public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS" ;

    public boolean isAnonymous() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .anyMatch(s -> s.equals(ROLE_ANONYMOUS));
    }

    public boolean isLoggedIn() {
        return !isAnonymous();
    }
}
