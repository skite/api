package fr.fstaine.skite.SkiteApi.services.booking.taken;

import fr.fstaine.skite.SkiteApi.entities.booking.TakenDates;
import org.springframework.lang.NonNull;

public interface BookingTakenService {

    @NonNull
    TakenDates getTakenDates();
}
