package fr.fstaine.skite.SkiteApi.services.booking.taken;

import fr.fstaine.skite.SkiteApi.entities.booking.Booking;
import fr.fstaine.skite.SkiteApi.entities.booking.TakenDates;
import fr.fstaine.skite.SkiteApi.repositories.booking.BookingRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BookingTakenServiceImpl implements BookingTakenService {

    private final BookingRepository repository;

    public BookingTakenServiceImpl(BookingRepository repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public TakenDates getTakenDates() {
        Stream<Booking> bookings = repository.findAllValidated();
        return bookings.map(this::getTakenDates).reduce(TakenDates.empty(), TakenDates::merge);
    }

    private TakenDates getTakenDates(Booking booking) {
        LocalDate start = booking.getStartDate();
        LocalDate end = booking.getEndDate();
        Set<LocalDate> dates = Stream.iterate(start, d -> !d.isAfter(end), d -> d.plusDays(1))
                .collect(Collectors.toUnmodifiableSet());
        return new TakenDates(dates);
    }
}
