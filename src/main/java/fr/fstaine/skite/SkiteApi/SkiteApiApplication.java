package fr.fstaine.skite.SkiteApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkiteApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkiteApiApplication.class, args);
	}

}
