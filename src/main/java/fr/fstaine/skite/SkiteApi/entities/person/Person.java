package fr.fstaine.skite.SkiteApi.entities.person;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Person {
    private @Id @GeneratedValue String id;
    private String lastName;
    private String firstName;

    public Person() {}

    public Person(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public String getId() {
        return this.id;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setId(String id) {
        this.id= id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Person))
            return false;
        Person person = (Person) obj;
        return Objects.equals(this.id, person.id) && Objects.equals(this.firstName, person.firstName) && Objects.equals(this.lastName, person.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.lastName, this.firstName);
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + this.id + ", lastName='" + this.lastName + '\'' + ", firstName='" + this.firstName + '\'' + '}';
    }
}
