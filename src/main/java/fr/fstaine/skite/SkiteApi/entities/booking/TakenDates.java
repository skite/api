package fr.fstaine.skite.SkiteApi.entities.booking;

import org.springframework.lang.NonNull;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class TakenDates {

    @NonNull
    private Set<LocalDate> dates;

    public TakenDates(@NonNull Set<LocalDate> dates) {
        this.dates = dates;
    }

    @NonNull
    public Set<LocalDate> getDates() {
        return dates;
    }

    public void setDates(@NonNull Set<LocalDate> dates) {
        this.dates = dates;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dates);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TakenDates that = (TakenDates) o;
        return dates.equals(that.dates);
    }

    @Override
    public String toString() {
        return "TakenDates{" +
                "dates=" + dates +
                '}';
    }

    public static TakenDates empty() {
        return new TakenDates(Collections.emptySet());
    }

    public TakenDates merge(TakenDates other) {
        Set<LocalDate> mergedDates = Stream.of(this.dates, other.dates)
                .flatMap(Collection::stream)
                .collect(Collectors.toUnmodifiableSet());
        return new TakenDates(mergedDates);
    }
}
