package fr.fstaine.skite.SkiteApi.entities.booking;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Booking {
    private @Id @GeneratedValue String id;

    private LocalDate startDate;
    private LocalDate endDate;
    private BookingStatus status;
    private String tenantId;

    public Booking() {}

    public Booking(LocalDate startDate, LocalDate endDate, BookingStatus status, String tenantId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.tenantId = tenantId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Booking))
            return false;
        Booking booking = (Booking) o;
        return Objects.equals(id, booking.id) &&
                Objects.equals(startDate, booking.startDate) &&
                Objects.equals(endDate, booking.endDate) &&
                Objects.equals(status, booking.status) &&
                Objects.equals(tenantId, booking.tenantId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startDate, endDate, status, tenantId);
    }

    @Override
    public String toString() {
        return "Booking{" +
                "id=" + id +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", status='" + status + '\'' +
                ", personId='" + tenantId + '\'' +
                '}';
    }
}
