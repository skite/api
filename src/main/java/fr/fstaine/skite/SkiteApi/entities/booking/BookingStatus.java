package fr.fstaine.skite.SkiteApi.entities.booking;

public enum BookingStatus {
    NEW,
    VALIDATED,
    CANCELLED
}
