package fr.fstaine.skite.SkiteApi.repositories.person;

import fr.fstaine.skite.SkiteApi.entities.person.Person;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PersonRepository extends MongoRepository<Person, String> {
}
