package fr.fstaine.skite.SkiteApi.repositories.booking;

import fr.fstaine.skite.SkiteApi.entities.booking.Booking;
import fr.fstaine.skite.SkiteApi.entities.booking.BookingStatus;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.stream.Stream;

public interface BookingRepository extends MongoRepository<Booking, String> {
    List<Booking> findByTenantId(String tenantId);

    Stream<Booking> findByStatus(BookingStatus status);

    default Stream<Booking> findAllValidated() {
        return this.findByStatus(BookingStatus.VALIDATED);
    }
}
