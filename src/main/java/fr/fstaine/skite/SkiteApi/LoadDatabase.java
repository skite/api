package fr.fstaine.skite.SkiteApi;

import fr.fstaine.skite.SkiteApi.entities.booking.Booking;
import fr.fstaine.skite.SkiteApi.entities.booking.BookingStatus;
import fr.fstaine.skite.SkiteApi.entities.person.Person;
import fr.fstaine.skite.SkiteApi.repositories.booking.BookingRepository;
import fr.fstaine.skite.SkiteApi.repositories.person.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;

@Configuration
@Profile("dev")
public class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(PersonRepository personRepository, BookingRepository bookingRepository) {
        return args -> {
            personRepository.deleteAll();
            Person flo = personRepository.save(new Person("Florian", "Staine"));
            Person sev = personRepository.save(new Person("Severine", "Staine"));

            personRepository.findAll().forEach(person -> log.info("Preloaded " + person));

            bookingRepository.deleteAll();
            bookingRepository.save(new Booking(LocalDate.of(2021, 5, 22), LocalDate.of(2021, 5, 28),
                    BookingStatus.VALIDATED, flo.getId()));
            bookingRepository.save(new Booking(LocalDate.of(2021, 5, 28), LocalDate.of(2021, 6, 5),
                    BookingStatus.NEW, sev.getId()));

            bookingRepository.findAll().forEach(booking -> log.info("Preloaded " + booking));
        };
    }
}
