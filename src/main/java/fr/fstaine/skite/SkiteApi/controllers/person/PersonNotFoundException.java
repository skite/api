package fr.fstaine.skite.SkiteApi.controllers.person;

public class PersonNotFoundException extends RuntimeException {
    PersonNotFoundException(String id) {
        super("Could not find person " + id);
    }
}
