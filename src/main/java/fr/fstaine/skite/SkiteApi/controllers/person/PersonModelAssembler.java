package fr.fstaine.skite.SkiteApi.controllers.person;

import fr.fstaine.skite.SkiteApi.controllers.booking.BookingController;
import fr.fstaine.skite.SkiteApi.entities.person.Person;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class PersonModelAssembler implements RepresentationModelAssembler<Person, EntityModel<Person>> {

    @Override
    public EntityModel<Person> toModel(Person person) {
        return EntityModel.of(person,
                linkTo(methodOn(PersonController.class).one(person.getId())).withSelfRel(),
                linkTo(methodOn(PersonController.class).all()).withRel("people"),
                linkTo(methodOn(BookingController.class).withTenant(person.getId())).withRel("bookings")
        );
    }
}
