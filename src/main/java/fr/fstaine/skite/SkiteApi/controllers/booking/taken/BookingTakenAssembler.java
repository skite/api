package fr.fstaine.skite.SkiteApi.controllers.booking.taken;

import fr.fstaine.skite.SkiteApi.controllers.booking.BookingController;
import fr.fstaine.skite.SkiteApi.entities.booking.TakenDates;
import fr.fstaine.skite.SkiteApi.services.auth.AuthenticationHelperService;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BookingTakenAssembler implements RepresentationModelAssembler<TakenDates, EntityModel<TakenDates>> {

    private final AuthenticationHelperService authenticationHelperService;

    public BookingTakenAssembler(AuthenticationHelperService authenticationHelperService) {
        this.authenticationHelperService = authenticationHelperService;
    }

    @NonNull
    @Override
    public EntityModel<TakenDates> toModel(@NonNull TakenDates entity) {
        var links = new ArrayList<Link>();
        links.add(linkTo(methodOn(BookingTakenController.class).takenDates()).withSelfRel());
        if (authenticationHelperService.isLoggedIn()) {
            links.add(linkTo(methodOn(BookingController.class).all()).withRel("bookings"));
        }
        return EntityModel.of(entity, links);
    }
}
