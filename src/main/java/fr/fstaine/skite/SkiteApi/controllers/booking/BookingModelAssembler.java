package fr.fstaine.skite.SkiteApi.controllers.booking;

import fr.fstaine.skite.SkiteApi.controllers.person.PersonController;
import fr.fstaine.skite.SkiteApi.entities.booking.Booking;
import fr.fstaine.skite.SkiteApi.entities.booking.BookingStatus;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BookingModelAssembler implements RepresentationModelAssembler<Booking, EntityModel<Booking>> {
    @NonNull
    @Override
    public EntityModel<Booking> toModel(@NonNull Booking booking) {
        EntityModel<Booking> model = EntityModel.of(booking,
                linkTo(methodOn(BookingController.class).one(booking.getId())).withSelfRel(),
                linkTo(methodOn(BookingController.class).all()).withRel("bookings"),
                linkTo(methodOn(PersonController.class).one(booking.getTenantId())).withRel("tenant")
        );
        if(booking.getStatus() == BookingStatus.NEW) {
            model.add(linkTo(methodOn(BookingController.class).validate(booking.getId())).withRel("validate"));
            model.add(linkTo(methodOn(BookingController.class).cancel(booking.getId())).withRel("cancel"));
        }

        return model;
    }

    @NonNull
    @Override
    public CollectionModel<EntityModel<Booking>> toCollectionModel(@NonNull Iterable<? extends Booking> bookings) {
        List<EntityModel<Booking>> bookingEntities = StreamSupport.stream(bookings.spliterator(), false)
                .map(this::toModel)
                .collect(Collectors.toList());
        return CollectionModel.of(bookingEntities,
                linkTo(methodOn(BookingController.class).all()).withSelfRel());
    }
}
