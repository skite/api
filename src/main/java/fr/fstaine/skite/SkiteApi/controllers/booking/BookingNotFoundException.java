package fr.fstaine.skite.SkiteApi.controllers.booking;

public class BookingNotFoundException extends RuntimeException {
    BookingNotFoundException(String id) {
        super("Could not find booking " + id);
    }
}
