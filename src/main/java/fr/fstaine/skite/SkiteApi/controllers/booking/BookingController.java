package fr.fstaine.skite.SkiteApi.controllers.booking;

import fr.fstaine.skite.SkiteApi.entities.booking.Booking;
import fr.fstaine.skite.SkiteApi.entities.booking.BookingStatus;
import fr.fstaine.skite.SkiteApi.repositories.booking.BookingRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookingController {

    private final BookingRepository repository;
    private final BookingModelAssembler assembler;

    public BookingController(BookingRepository repository, BookingModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/bookings")
    public CollectionModel<EntityModel<Booking>> all() {
        return assembler.toCollectionModel(repository.findAll());
    }

    @GetMapping("/bookings/{id}")
    public EntityModel<Booking> one(@PathVariable String id) {
        Booking booking = repository.findById(id)
                .orElseThrow(() -> new BookingNotFoundException(id));
        return assembler.toModel(booking);
    }

    @PostMapping("/bookings")
    public ResponseEntity<EntityModel<Booking>> newBooking(@RequestBody Booking booking) {
        booking.setStatus(BookingStatus.NEW);
        Booking newBooking = repository.save(booking);
        EntityModel<Booking> entityModel = assembler.toModel(newBooking);

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @GetMapping("/bookings/{id}/validate")
    public ResponseEntity<?> validate(@PathVariable String id) {
        Booking booking = repository.findById(id)
                .orElseThrow(() -> new BookingNotFoundException(id));

        if(booking.getStatus() == BookingStatus.NEW) {
            booking.setStatus(BookingStatus.VALIDATED);
            return ResponseEntity.ok(assembler.toModel(repository.save(booking)));
        } else {
            Problem problem = Problem.create()
                    .withTitle("Method not allowed")
                    .withDetail("You can't validate a booking in " + booking.getStatus() + " status");
            return ResponseEntity
                    .status(HttpStatus.METHOD_NOT_ALLOWED)
                    .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE)
                    .body(problem);
        }
    }

    @GetMapping("/bookings/{id}/cancel")
    public ResponseEntity<?> cancel(@PathVariable String id) {
        Booking booking = repository.findById(id)
                .orElseThrow(() -> new BookingNotFoundException(id));

        if(booking.getStatus() == BookingStatus.NEW) {
            booking.setStatus(BookingStatus.CANCELLED);
            return ResponseEntity.ok(assembler.toModel(repository.save(booking)));
        } else {
            Problem problem = Problem.create()
                    .withTitle("Method not allowed")
                    .withDetail("You can't cancel a booking in " + booking.getStatus() + " status");
            return ResponseEntity
                    .status(HttpStatus.METHOD_NOT_ALLOWED)
                    .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE)
                    .body(problem);
        }
    }

    @GetMapping("/people/{tenantId}/bookings")
    public CollectionModel<EntityModel<Booking>> withTenant(@PathVariable String tenantId) {
        return assembler.toCollectionModel(repository.findByTenantId(tenantId));
    }
}
