package fr.fstaine.skite.SkiteApi.controllers.booking.taken;

import fr.fstaine.skite.SkiteApi.entities.booking.TakenDates;
import fr.fstaine.skite.SkiteApi.services.booking.taken.BookingTakenService;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookingTakenController {

    private final BookingTakenService service;
    private final BookingTakenAssembler assembler;

    public BookingTakenController(BookingTakenService service, BookingTakenAssembler assembler) {
        this.service = service;
        this.assembler = assembler;
    }

    @CrossOrigin
    @GetMapping("/bookings/taken")
    public ResponseEntity<EntityModel<TakenDates>> takenDates() {
        TakenDates takenDates = service.getTakenDates();
        EntityModel<TakenDates> model = assembler.toModel(takenDates);
        return ResponseEntity.ok(model);
    }
}
